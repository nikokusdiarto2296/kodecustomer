# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Kode Customer',
    'version': '1.2',
    'category': 'Custom',
    'summary': 'Custom Module',
    'description': "Tugas Nurosoft Mengenai Kode Customer",
    'website': 'https://www.odoo.com/',
    'author': 'Niko Kusdiarto',
    'depends': ['base', 'sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/customer_type_views.xml',
        'views/customer_partner.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'LGPL-3',
}
