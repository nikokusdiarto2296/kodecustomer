"""file customer type"""
from odoo import models, fields, api, _

class Customertype(models.Model):
    """new model"""
    _name       = "customertype"
    _rec_name = "customer_type_name"

    """field from customer type"""
    customer_type_name = fields.Char()
    customer_type_mode = fields.Selection([
        ('generate_code', 'Generate Code'),
        ('single_code', 'Single Code'),
        ('manual_code', 'Manual Code'),
    ], default="Generate Code")
    sequence_customer_id = fields.Many2one('ir.sequence')
    next_number_seq = fields.Integer(related='sequence_customer_id.number_next_actual')
    customer_code = fields.Char()

    @api.onchange("sequence_customer_id")
    def _change_sequence_customer(self):   
        for rec in self:
            if rec.customer_type_mode == 'generate_code':
                rec.customer_code = rec.sequence_customer_id.prefix
