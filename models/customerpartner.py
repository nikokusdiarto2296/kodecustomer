"""file customer partner"""
from odoo import models, fields, api, _

class Customerpartner(models.Model):
    """inherit model"""
    _inherit = 'res.partner'

    """field from customer partner"""
    customertype_id = fields.Many2one('customertype')
    customer_type = fields.Char()
    customer_code = fields.Char(tracking=True)
    history_codecustomer_generate = fields.Char()
    history_codecustomer_manual = fields.Char()
    temp_codemanual = fields.Char()

    @api.onchange("customertype_id")
    def _change_customer(self):   
        for rec in self:
            rec.customer_type = rec.customertype_id.customer_type_mode
            if rec.customertype_id.customer_type_mode == 'generate_code':
                rec.customer_type = rec.customertype_id.customer_type_mode
                if rec.history_codecustomer_generate:
                    rec.customer_code = str(rec.history_codecustomer_generate)
                else:
                    rec.customer_code = str(rec.customertype_id.sequence_customer_id.next_by_id())
                    rec.history_codecustomer_generate = str(rec.customer_code)
            elif rec.customertype_id.customer_type_mode == 'single_code':
                rec.customer_type = rec.customertype_id.customer_type_mode
                rec.customer_code = rec.customertype_id.customer_code
            elif rec.customertype_id.customer_type_mode == 'manual_code':
                rec.customer_type = rec.customertype_id.customer_type_mode
                if rec.temp_codemanual:
                    rec.customer_code = rec.temp_codemanual
                else:
                    rec.customer_code = rec.history_codecustomer_manual

                if rec.history_codecustomer_manual:
                    rec.customer_code = str(rec.history_codecustomer_manual)
                else:
                    rec.history_codecustomer_manual = rec.customer_code
    
    @api.onchange("customer_code")
    def _change_codemanual(self):
        for rec in self:
            if rec.customertype_id.customer_type_mode == 'manual_code':
                rec.temp_codemanual = rec.customer_code
    
    @api.model
    def create(self, vals):
        res = super(Customerpartner, self).create(vals)
        res.temp_codemanual = ''
        if res.customer_type == 'manual_code':
            res.history_codecustomer_manual = res.customer_code
        return res